const chalk = require('chalk');

function generateSpacer(comparativeStrings, targetString, specialChar) {
  const maxChar = comparativeStrings.reduce(
    (prev, curr) => (prev.length > curr.length ? prev : curr),
  ).length;

  let spaces = '';

  for (let i = 0; i < maxChar - targetString.length; i += 1) {
    spaces += specialChar || ' ';
  }

  return spaces;
}

function interpolateTimestamp(string) {
  const now = new Date();

  const year = now.getFullYear();
  const month = now.getMonth();
  const date = now.getDate();
  const day = now.getDay();
  const militaryHour = now.getHours();
  let min = now.getMinutes();
  let sec = now.getSeconds();
  const amOrPm = militaryHour < 12 ? 'AM' : 'PM';

  let hour = militaryHour < 12 ? militaryHour : militaryHour - 12;
  hour = hour || 12;

  if (min < 10) min = `0${min}`;
  if (sec < 10) sec = `0${sec}`;

  return string
    .replace(/<year>/g, year)
    .replace(/<month>/g, month)
    .replace(/<date>/g, date)
    .replace(/<day>/g, day)
    .replace(/<hour>/g, hour)
    .replace(/<military-hour>/g, militaryHour)
    .replace(/<min>/g, min)
    .replace(/<sec>/g, sec)
    .replace(/<am\|pm>/g, amOrPm)
    .replace(/<timestamp>/g, chalk.gray(`${hour}:${min}:${sec} ${amOrPm}`))
    .replace(
      /<military-timestamp>/g,
      chalk.gray(`${militaryHour}:${min}:${sec}`),
    );
}

function format(achooInst, meta, ...msg) {
  const config = achooInst.config;
  const logger = achooInst.loggers[meta.logger];
  const level = achooInst.levels[meta.level];

  const idColor = chalk[level.color] || chalk.stripColor;

  const formatRule = logger.format || '<time> <id> <msg>';

  const namespace = `${config.name.toLowerCase()}:`;
  const loggerLabel = meta.logger === 'default'
    ? ''
    : `${meta.logger.toLowerCase()}:`;
  const levelLabel = meta.level ? `${meta.level.toLowerCase()}` : '';

  const ID = idColor(
    `${namespace}${loggerLabel}${levelLabel}${generateSpacer(
      Object.keys(achooInst.levels),
      levelLabel,
    )}`,
  );
  const MSG = msg.join(' ');

  let output = interpolateTimestamp(
    formatRule.replace(/<id>/g, ID).replace(/<msg>/g, MSG),
  );

  Object.keys(meta).forEach(key => {
    const keyWithoutIterator = key.replace(/@[0-9]+/g, '');
    const transform =
      achooInst.transforms[keyWithoutIterator] ||
      logger.transforms[keyWithoutIterator];
    if (transform) {
      const args = !Array.isArray(meta[key]) ? [meta[key]] : [...meta[key]];
      output = output.replace(new RegExp(`<${key}>`, 'g'), transform(...args));
    }
  });

  return `${output}\n`;
}

module.exports = format;
