const formatter = require('./formatter');
const fs = require('fs');
const path = require('path');
const merge = require('lodash').merge;

const CWD = fs.realpathSync(process.cwd());
const pathToConfig = path.join(CWD, 'achoo.json');
const defaultConfig = require('./defaults.json');

let instance;

class Achoo {
  constructor() {
    if (!instance) {
      instance = this;
    } else {
      return instance;
    }

    this.config = Achoo.parseConfig();

    this.loggers = {};
    this.levels = {};
    this.transforms = {};

    Object.keys(this.config.loggers).forEach(loggerName => {
      this.addLogger(loggerName, this.config.loggers[loggerName]);
    });

    this.default = this.loggers.default;

    Object.keys(this.config.levels).forEach(levelName => {
      this.addLevel(levelName, this.config.levels[levelName]);
    });

    return this;
  }

  log(meta, ...msg) {
    Achoo.generateMessage(meta, 'default', 'info', ...msg);
    return this;
  }

  addLogger(name, config) {
    const logger = config || {};

    this.loggers[name] = logger;

    logger.log = (meta, ...msg) => {
      Achoo.generateMessage(meta, name, 'info', ...msg);
    };

    Object.keys(this.levels).forEach(levelName => {
      logger[levelName] = (meta, ...msg) => {
        Achoo.generateMessage(meta, name, levelName, ...msg);
        return this;
      };
    });

    logger.transforms = {};

    logger.addTransform = (transformName, callback) =>
      (logger.transforms[transformName] = callback);

    return this.getLogger(name);
  }

  addLevel(name, config) {
    const level = config || {};

    this.levels[name] = level;

    Object.keys(this.loggers).forEach(loggerName => {
      this.loggers[loggerName][name] = (meta, ...msg) => {
        Achoo.generateMessage(meta, loggerName, name, ...msg);
        return this;
      };
    });

    this[name] = this.default[name];
  }

  getLogger(loggerName) {
    return this.loggers[loggerName];
  }

  addTransform(name, callback) {
    this.transforms[name] = callback;
  }

  static writeOut(meta, ...msg) {
    const formatted = formatter(instance, meta, ...msg);

    switch (instance.loggers[meta.logger].output) {
      case 'process.stdout': {
        return process.stdout.write(formatted);
      }

      case 'process.stderr': {
        return process.stderr.write(formatted);
      }

      default: {
        return process.stdout.write(formatted);
      }
    }
  }

  static generateMessage(meta, defaultLoggerName, defaultLevelName, ...msg) {
    const defaultMeta = { logger: defaultLoggerName, level: defaultLevelName };

    if (typeof meta === 'string') {
      const firstString = meta;
      Achoo.writeOut(defaultMeta, firstString, ...msg);
      return;
    }

    const metaOut = merge(meta, defaultMeta);

    Achoo.writeOut(metaOut, ...msg);
  }

  static parseConfig() {
    let userConfig;

    if (fs.existsSync(pathToConfig)) {
      userConfig = require(pathToConfig);
      return merge({}, defaultConfig, userConfig);
    }

    return defaultConfig;
  }
}

module.exports = new Achoo();
